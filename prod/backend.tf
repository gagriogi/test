terraform {
  backend "http" {
    #    skip_cert_verification = true
  }

  required_providers {
    local = {
      source  = "hashicorp/local"
      version = "2.0.0"
    }
  }
}
